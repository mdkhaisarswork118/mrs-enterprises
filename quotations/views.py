from rest_framework import generics
from .models import Quotation, Bill
from .serializers import QuotationSerializer, UserSerializer, BillSerializer
from django.contrib.auth.models import User
from django.views import View
from django.http import JsonResponse
from django.db.models import Sum

class QuotationList(generics.ListCreateAPIView):
    queryset = Quotation.objects.all()
    serializer_class = QuotationSerializer

class QuotationDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Quotation.objects.all()
    serializer_class = QuotationSerializer


class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class BillList(generics.ListCreateAPIView):
    queryset = Bill.objects.all()
    serializer_class = BillSerializer

class BillDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Bill.objects.all()
    serializer_class = BillSerializer


class Dashboard(View):
    def get(self, request):
        total_quotations = Quotation.objects.count()
        total_bills = Bill.objects.count()
        orders = Bill.objects.select_related('quotation').count()
        revenue = Quotation.objects.select_related('quotation') .aggregate(Sum('total_price'))['total_price__sum']
        recent_quotations = Quotation.objects.order_by('-id')[:5]
        recent_bills = Quotation.objects.filter(bill__isnull=False).order_by('-id')[:5]
        
        data = [{'total_quotations': total_quotations, 'total_bills':total_bills, 'orders':orders, 'revenue':revenue, 'recent_quotations':list(recent_quotations.values()), 'recent_bills':list(recent_bills.values())}]

        return JsonResponse(data, safe=False)
