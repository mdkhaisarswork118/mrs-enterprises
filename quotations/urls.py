from django.urls import path
from .views import QuotationList, QuotationDetail, UserList, UserDetail, BillList, BillDetail, Dashboard

urlpatterns = [
    path('', Dashboard.as_view(), name='dashboard'),
    path('quotations/', QuotationList.as_view(), name='quotation-list'),
    path('quotations/<int:pk>/', QuotationDetail.as_view(), name='quotation-detail'),
    path('users/', UserList.as_view(), name='user-list'),
    path('users/<int:pk>/', UserDetail.as_view(), name='user-detail'),
    path('bills/', BillList.as_view(), name='bill-list'),
    path('bills/<int:pk>/', BillDetail.as_view(), name='bill-detail'),
]
