from django.contrib import admin
from .models import Bill, Quotation, QuotationItem
from django.contrib.auth.models import User

# Register your models here.

admin.site.register(Bill)
admin.site.register(Quotation)
admin.site.register(QuotationItem)
